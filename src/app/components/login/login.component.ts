import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    @Input() defaultHost: string;
    @Input() defaultUsername: string;
    @Input() defaultPassword: string;
    @Output() onLogin = new EventEmitter();
    @Output() onLogout = new EventEmitter();

    private readonly AUTH_ENDPOINT = '/rest/auth/1/session';
    private readonly MYSELF_ENDPOINT = '/rest/api/2/myself';
    public username = '';
    public password = '';
    public host = '';

    isLogged = false;
    message = null;
    error = false;
    spinner = false;

    constructor(private http: HttpClient) { }

    ngOnInit() {
        if (this.defaultUsername) {
            this.username = this.defaultUsername;
        }

        if (this.defaultPassword) {
            this.password = this.defaultPassword;
        }

        if (!this.defaultHost) {
            // throw new Error('Attribute \'defaultHost\' is required');
            return;
        }

        this.host = this.defaultHost;

        this.spinner = true;

        const url = this.host + this.AUTH_ENDPOINT;

        this.http.get(url)
            .pipe(finalize(() => this.spinner = false))
            .subscribe(result => {
                const username = result['name'];

                this.onLogin.emit({
                    username: this.username,
                    password: this.password,
                    host: this.host
                });

                this.isLogged = true;
                this.error = false;
                this.message = `You're now logged in as ${username}`;
            }, err => { });
    }

    login() {
        this.spinner = true;

        const headers = {
            Authorization: 'Basic ' + btoa(`${this.username}:${this.password}`)
        };

        const url = this.host + this.MYSELF_ENDPOINT;

        this.http.get(url, { headers })
            .pipe(finalize(() => this.spinner = false))
            .subscribe(result => {

                const username = result['key'];

                this.onLogin.emit({
                    username: this.username,
                    password: this.password,
                    host: this.host
                });

                this.isLogged = true;
                this.error = false;
                this.message = `You're now logged in as ${username}`;
            }, error => {
                console.log(error);
                this.error = true;

                let errMsg;
                if (error.status === 401) {
                    errMsg = 'Incorrect username and\/or password.';
                } else {
                    errMsg = 'An unexpected error has occurred.';
                }

                this.message = errMsg;
            });
    }

    logout() {
        this.spinner = true;

        const url = this.host + this.AUTH_ENDPOINT;

        this.http.delete(url)
            .pipe(finalize(() => {
                this.spinner = false;
                this.onLogout.emit(undefined);
                this.reset();
            }))
            .subscribe();
    }

    public reset() {
        this.username = '';
        this.password = '';
        this.isLogged = false;
        this.error = false;
        this.message = null;
        this.spinner = false;

        if (this.defaultHost) {
            this.host = this.defaultHost;
        }
    }
}

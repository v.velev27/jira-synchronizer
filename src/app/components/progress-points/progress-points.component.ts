import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-points',
  templateUrl: './progress-points.component.html',
  styleUrls: ['./progress-points.component.css']
})
export class ProgressPointsComponent implements OnInit {
  @Input() progressPoints: any[];

  constructor() { }

  ngOnInit() {
  }

}

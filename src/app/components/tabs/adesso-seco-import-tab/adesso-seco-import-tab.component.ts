import { Component, OnInit } from '@angular/core';
import { AdessoSecoImportService } from 'app/services/adesso-seco-import.service';
import { Auth } from 'app/domain/auth';

@Component({
	selector: 'app-adesso-seco-import-tab',
	templateUrl: './adesso-seco-import-tab.component.html',
	styleUrls: ['./adesso-seco-import-tab.component.css']
})
export class AdessoSecoImportTabComponent implements OnInit {
	constructor(private importer: AdessoSecoImportService) { }

	ngOnInit() { }

	setImportLogin(auth: Auth) {
		this.importer.setImportAuth(auth);
	}

	setExportLogin(auth: Auth) {
		this.importer.setExportAuth(auth);
	}

	handleFileInput(files: FileList) {
		this.importer.setExportfile(files.item(0));
	}
}

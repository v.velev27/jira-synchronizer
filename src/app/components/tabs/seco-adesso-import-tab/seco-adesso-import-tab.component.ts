import { Component, OnInit } from '@angular/core';
import { SecoAdessoImportService } from 'app/services/seco-adesso-import.service';

@Component({
  selector: 'app-seco-adesso-import-tab',
  templateUrl: './seco-adesso-import-tab.component.html',
  styleUrls: ['./seco-adesso-import-tab.component.css']
})
export class SecoAdessoImportTabComponent implements OnInit {

  constructor(private importer: SecoAdessoImportService) { }

  ngOnInit() {
  }

  setImportLogin(auth) {
    this.importer.setImportLogin(auth);
  }

  setExportLogin(auth) {
    this.importer.setExportLogin(auth);
  }

  handleFileInput(files: FileList) {
    this.importer.setExportfile(files.item(0));
  }

}

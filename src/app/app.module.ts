import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { ProgressPointsComponent } from './components/progress-points/progress-points.component';
import { SecoAdessoImportTabComponent } from './components/tabs/seco-adesso-import-tab/seco-adesso-import-tab.component';
import { AdessoSecoImportTabComponent } from './components/tabs/adesso-seco-import-tab/adesso-seco-import-tab.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProgressPointsComponent,
    SecoAdessoImportTabComponent,
    AdessoSecoImportTabComponent
  ],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { AttachmentsDBService } from './attachments-db.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { mergeMap, retryWhen, take, delay } from 'rxjs/operators';
import { Attachment } from '../../domain/attachment';
import { Auth } from '../../domain/auth';

@Injectable({
    providedIn: 'root'
})
export class AttachmentsTransferService {

    private readonly RETRY_DELAY = 500;
    private readonly RETRY_TAKES = 3;
    private readonly MAX_CONCURRENT_REQUESTS = 20;

    constructor(private attachmentsDB: AttachmentsDBService, private http: HttpClient) { }

    downloadAttachments(attachmentsMeta, auth: Auth) {

        // set headers with basic auth
        const headers = new HttpHeaders({
            Authorization: 'Basic ' + btoa(`${auth.username}:${auth.password}`)
        });

        // create the requests
        return from(attachmentsMeta)
            .pipe(mergeMap<any, Observable<any>>((attachmentMeta) => {
                return new Observable(observer => {
                    // download each file
                    this.http.get(attachmentMeta.fileLink, { responseType: 'blob', headers }).subscribe(result => {
                        // create new attachment object
                        const newAttachment: Attachment = {
                            data: result,
                            issueKey: attachmentMeta.issueKey,
                            fileName: attachmentMeta.fileName
                        };

                        // save the attachment to the db and resolve
                        this.attachmentsDB.save(newAttachment)
                            .finally(() => {
                                observer.next(result);
                                observer.complete();
                            });
                    }, err => {
                        observer.next();
                        observer.complete();
                    });
                });
            }));
    }

    uploadAttachments(attachments: Attachment[], auth: Auth) {
        // set headers with basic auth
        const headers = new HttpHeaders({
            'X-Atlassian-Token': 'no-check',
            Authorization: 'Basic ' + btoa(`${auth.username}:${auth.password}`),
        });

        // convert attachments to Observable values
        return from(attachments)
            // merge all requests into a single Observable
            .pipe(mergeMap<any, Observable<any>>((attachment: Attachment, index) => {

                // set file in body
                const formData = new FormData();
                formData.append('file', attachment.data, attachment.fileName);
                // set the request
                return this.http
                    .post(`${auth.host}/rest/api/latest/issue/${attachment.issueKey}/attachments`,
                        formData,
                        { headers })
                    .pipe(
                        // retry request in case there's an error (e.g. server under heavy load, too many requests)
                        retryWhen(errors => {
                            console.log(errors);
                            return errors.pipe(delay(this.RETRY_DELAY), take(this.RETRY_TAKES));
                        })
                    );
                // set max concurrent upload requests
            }, this.MAX_CONCURRENT_REQUESTS)
            );
    }
}

import { Injectable } from '@angular/core';
import { Attachment } from '../../domain/attachment';

@Injectable({
    providedIn: 'root'
})
export class AttachmentsDBService {

    readonly IDB_DB = 'attachments_db';
    readonly IDB_OBJECTSTORE = 'attachments';
    private db;

    constructor() { }

    save(attachment: Attachment): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.getObjectStore().then((objectStore) => {
                const request = objectStore.add(attachment);

                request.onsuccess = (result) => {
                    resolve(result);
                };

                request.onerror = request.onsuccess;
            });
        });
    }

    get(fileName): Promise<Attachment> {
        return new Promise<any>((resolve, reject) => {
            this.getObjectStore().then((objectStore) => {
                objectStore.get(fileName).onsuccess = (event: any) => {
                    resolve(event.target.result);
                };
            });
        });
    }

    saveMany(attachments: Attachment[]) {
        return new Promise<any>((mainResolve, reject) => {
            Promise
                .all(attachments.map(attachment => this.save(attachment)))
                .then(mainResolve);
        });
    }

    getMany(fileNames: any[]): Promise<Attachment[]> {
        return new Promise<any>((mainResolve, reject) => {
            const results = {};
            Promise
                .all(fileNames.map((fileName => {
                    return new Promise((resolve) => {
                        this.get(fileName).then((data) => {
                            results[fileName] = data;
                            resolve();
                        });
                    });
                })))
                .then(() => mainResolve(results));
        });
    }

    getAll(): Promise<Attachment[]> {
        return new Promise<any>((resolve, reject) => {
            this.getObjectStore().then((objectStore) => {
                objectStore.getAll().onsuccess = (event: any) => {
                    resolve(event.target.result);
                };
            });
        });
    }

    private getDb(): Promise<IDBDatabase> {
        return new Promise((resolve, reject) => {

            if (!this.db) {
                const dbRequest = indexedDB.open(this.IDB_DB, 1);

                dbRequest.onsuccess = (event: any) => {
                    resolve(event.target.result);
                };

                dbRequest.onupgradeneeded = (event: any) => {
                    event.currentTarget.result.createObjectStore(this.IDB_OBJECTSTORE, { autoIncrement: true });
                };
            } else {
                resolve(this.db);
            }
        });
    }

    private getObjectStore(): Promise<IDBObjectStore> {
        return new Promise((resolve, reject) => {
            this.getDb().then((db) => {
                resolve(db.transaction(this.IDB_OBJECTSTORE, 'readwrite').objectStore(this.IDB_OBJECTSTORE));
            });
        });
    }

    public closeDb() {
        if (this.db) {
            this.db.close();
        }
    }

    public deleteDb() {
        return indexedDB.deleteDatabase(this.IDB_DB);
    }

    public clearDb() {
        return new Promise((resolve) => {
            this.getObjectStore().then((objectStore) => {
                resolve(objectStore.clear());
            });
        });
    }
}

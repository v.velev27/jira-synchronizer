import { Injectable } from '@angular/core';
import FileDataLoader from '../utils/file-data-loader';
import { AttachmentsTransferService } from './attachments/attachments-transfer.service';
import { AttachmentsDBService } from './attachments/attachments-db.service';
import { Attachment } from '../domain/attachment';
import { from, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { JiraService } from './jira/jira.service';
import { ProgressService, ProgressStatus } from './progress.service';
import { Auth } from 'app/domain/auth';

@Injectable({
    providedIn: 'root'
})
export class SecoAdessoImportService {
    private readonly MAX_CONCURRENT_REQUESTS = 20;

    private sourceAuth: Auth;
    private targetAuth: Auth;

    private exportFile;

    constructor(
        private attachmentsSvc: AttachmentsTransferService,
        private attachmentsDbSvc: AttachmentsDBService,
        private jiraSvc: JiraService,
        private progress: ProgressService
    ) { }

    setExportLogin(exportLogin: Auth) {
        this.sourceAuth = exportLogin;
    }

    setImportLogin(importLogin: Auth) {
        this.targetAuth = importLogin;
    }

    setExportfile(file: File) {
        this.exportFile = file;
    }

    getExportFile() {
        return this.exportFile;
    }

    async import() {
        let existingIssues: object[] = [];
        let uploadAttachmentsMeta: Attachment[] = [];
        let exportIssues: object[] = [];
        let attachmentIdsForDelete: string[] = [];
        let keysMap: any[] = [];

        // setting auth (username, password, host)
        this.jiraSvc.setAuth(this.targetAuth);

        // loading the existing issues from the target Jira
        this.progress.set('loadingExisting', ProgressStatus.LOADING);
        const issues = await this.loadExistingIssues();
        existingIssues = issues;
        this.progress.set('loadingExisting', ProgressStatus.DONE);


        // loading objects from file
        this.progress.set('mapping', ProgressStatus.LOADING);
        const fileIssues = await FileDataLoader.load(this.exportFile);
        exportIssues = fileIssues;
        // filtering the issues for create and update
        const issuesForCreate = exportIssues.filter(issue => !issue['Custom field (Ticket-URL Adesso)']);
        const issuesForUpdate = exportIssues.filter(issue => issue['Custom field (Ticket-URL Adesso)']);
        // getting the attachment ids that need to be deleted
        attachmentIdsForDelete = this.extractAttachmentIdsForDelete(existingIssues, issuesForUpdate);
        this.progress.set('mapping', ProgressStatus.DONE);


        // creating issues
        this.progress.set('createIssues', ProgressStatus.LOADING, 0, issuesForCreate.length);
        const result = await this.createIssues(issuesForCreate);
        keysMap = result['keysMap'];
        // saving the attachments meta
        uploadAttachmentsMeta = uploadAttachmentsMeta.concat(result['issuesAttachmentsMeta']);
        this.progress.set('createIssues', ProgressStatus.DONE);


        // updating issues
        this.progress.set('updateIssues', ProgressStatus.LOADING, 0, issuesForUpdate.length);
        const attachmentsMeta = await this.updateIssues(issuesForUpdate, existingIssues);
        // filtering only non-existing attachments
        const updateAttachmentsMeta: Attachment[] = attachmentsMeta.filter((attachment: Attachment) =>
            existingIssues
                .find(existingIssue => existingIssue['key'] === attachment.issueKey)['fields']['attachment']
                .find(issueAttachment => issueAttachment['filename'] === attachment.fileName));
        // saving those attachment metas
        uploadAttachmentsMeta = uploadAttachmentsMeta.concat(updateAttachmentsMeta);
        this.progress.set('updateIssues', ProgressStatus.DONE);


        // downloading attachments
        this.progress.set('downloadAttachments', ProgressStatus.LOADING, 0, uploadAttachmentsMeta.length);
        await Promise.all([
            this.downloadAttachments(uploadAttachmentsMeta),
            this.deleteUpdateAttachments(attachmentIdsForDelete)
        ]);
        // getting attachment references from DB
        const attachments = await this.attachmentsDbSvc.getAll();
        this.progress.set('downloadAttachments', ProgressStatus.DONE);


        // uploading attachments
        this.progress.set('uploadAttachments', ProgressStatus.LOADING, 0, attachments.length);
        await this.uploadAttachments(attachments);
        this.progress.set('uploadAttachments', ProgressStatus.DONE);

        this.progress.set('updateAdessoKeys', ProgressStatus.LOADING, 0, keysMap.length);
        await this.updateAdessoKeys(keysMap);
        this.progress.set('updateAdessoKeys', ProgressStatus.DONE);
    }

    loadExistingIssues(): Promise<object[]> {
        return this.jiraSvc.getAllIssues(['attachment', 'priority', 'status']).toPromise();
    }

    createIssues(exportIssues): Promise<any> {
        return new Promise((resolve) => {
            let issuesAttachmentsMeta = [];
            const keysMap = [];
            // convert issue objects to Observable values
            from(exportIssues)
                // create create/update requests and merge them into a single observable
                .pipe(
                    mergeMap<any, Observable<any>>((issue) => {
                        return this.jiraSvc.createIssue(issue);
                    }, this.MAX_CONCURRENT_REQUESTS)
                ).subscribe(response => {
                    this.progress.increment('createIssues');

                    // extract attachments metadata for each issue
                    const issueAttachmentsMeta = this.extractIssueAttachmentsMeta(response.requestIssue);

                    if (issueAttachmentsMeta) {
                        issueAttachmentsMeta.forEach(attachment => attachment['issue'] = response.newIssue['key']);
                        issuesAttachmentsMeta = issuesAttachmentsMeta.concat(issueAttachmentsMeta);
                    }

                    keysMap.push({
                        secoKey: response.requestIssue['Issue key'],
                        adessoKey: response.newIssue['key']
                    });

                }, err => { },
                    () => {
                        resolve({ issuesAttachmentsMeta, keysMap });
                    });
        });
    }

    updateIssues(exportIssues, existingIssues): Promise<Attachment[]> {
        return new Promise((resolve) => {
            let issuesAttachmentsMeta = [];
            // convert issue objects to Observable values
            from(exportIssues)
                // create create/update requests and merge them into a single observable
                .pipe(
                    mergeMap<any, Observable<any>>((issue) => {
                        // tslint:disable-next-line: max-line-length
                        const existingIssue = existingIssues.find(mapExistingIssue => mapExistingIssue['key'] === this.extractAdessoKey(issue));
                        return this.jiraSvc.updateIssue(existingIssue, issue);
                    }, this.MAX_CONCURRENT_REQUESTS)
                ).subscribe(response => {
                    this.progress.increment('upldateIssues');

                    const issueAttachmentsMeta = this.extractIssueAttachmentsMeta(response.requestIssue);

                    if (issueAttachmentsMeta) {
                        issueAttachmentsMeta.forEach(attachment => attachment['issue'] = response['key']);
                        issuesAttachmentsMeta = issuesAttachmentsMeta.concat(issueAttachmentsMeta);
                    }

                }, err => { },
                    () => {
                        resolve(issuesAttachmentsMeta);
                    });
        });
    }

    deleteUpdateAttachments(ids) {
        return new Promise(resolve => {
            this.jiraSvc.deleteAllAttachments(ids).subscribe(() => { }, err => { }, resolve);
        });
    }

    downloadAttachments(issuesAttachmentsMeta) {

        return new Promise((resolve) => {
            this.attachmentsSvc.downloadAttachments(issuesAttachmentsMeta, this.sourceAuth)
                .subscribe(result => {
                    this.progress.increment('downloadAttachments');
                }, err => { }, resolve);
        });
    }

    uploadAttachments(attachments) {
        return new Promise((resolve) => {
            this.attachmentsSvc.uploadAttachments(attachments, this.targetAuth)
                .subscribe(result => {
                    this.progress.increment('uploadAttachments');
                    console.log('uploaded ' + result[0]['filename'] + ' for issue ');
                },
                    err => { },
                    () => {
                        this.progress.set('uploadAttachments', ProgressStatus.DONE)
                        console.log('All done!');
                        resolve();
                    });
        });
    }

    updateAdessoKeys(keysMap: { secoKey: string, adessoKey: string }[]) {
        return new Promise((resolve) => {
            // convert issue objects to Observable values
            from(keysMap)
                // create create/update requests and merge them into a single observable
                .pipe(
                    mergeMap<any, Observable<any>>((keyMap) => {
                        // tslint:disable-next-line: max-line-length
                        this.jiraSvc.setAuth(this.sourceAuth);
                        return this.jiraSvc.updateAdessoKey(keyMap.adessoKey, keyMap.secoKey);
                    }, this.MAX_CONCURRENT_REQUESTS)
                ).subscribe(response => {
                    this.progress.increment('updateAdessoKeys');
                }, err => { },
                    () => {
                        this.progress.set('updateAdessoKeys', ProgressStatus.DONE);
                        resolve();
                    });
        });
    }

    extractIssueAttachmentsMeta(issue) {
        const attachmentsMeta = [];

        if (issue.Attachment) {
            let attachmentString = issue.Attachment.split(';');
            attachmentsMeta.push({
                fileName: attachmentString[2],
                fileLink: attachmentString[3]
            });

            for (let i = 1; ; i++) {
                if (!issue['Attachment__' + i]) {
                    return attachmentsMeta;
                }

                attachmentString = issue['Attachment__' + i].split(';');
                attachmentsMeta.push({
                    fileName: attachmentString[2],
                    fileLink: attachmentString[3]
                });
            }
        } else {
            return attachmentsMeta;
        }
    }

    canImport() {
        return (this.sourceAuth && this.targetAuth && this.exportFile);
    }

    getProgress() {
        this.progress.get();
    }

    private extractAttachmentIdsForDelete(existingIssue, issuesForUpdate): any[] {
        return existingIssue.filter(issue => issuesForUpdate.find(issueForUpdate => this.extractAdessoKey(issueForUpdate) === issue['key'])
            && issue['fields']['attachment'].length > 0)
            .map(issue => {
                return issue['fields']['attachment'];
            })
            .flat()
            .map(attachment => attachment['id']);
    }

    private extractAdessoKey(issueForUpdate) {
        if (issueForUpdate['Custom field (Ticket-URL Adesso)']) {
            return issueForUpdate['Custom field (Ticket-URL Adesso)'].split('/').pop()
        } else {
            return null;
        }
    }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Auth } from 'app/domain/auth';
import { Observable, from } from 'rxjs';
import { retryWhen, take, mergeMap, delay } from 'rxjs/operators';
import { JiraIssueRequestBody } from 'app/domain/jira-issue-request-body';

@Injectable({
	providedIn: 'root'
})
export class JiraClient {
	private readonly ISSUES_ENDPOINT = '/rest/api/latest/issue/';
	private readonly SEARCH_ENDPOINT = '/rest/api/latest/search/';
	private readonly ATTACHMENTS_ENDPOINT = '/rest/api/latest/attachment/';
	private readonly TRANSITIONS_ENDPOINT = '/transitions/';
	private readonly ADESSO_JIRA_BROWSE = 'https://ticketing.adesso.ch/browse/';

	private readonly RETRY_DELAY = 500;
	private readonly RETRY_TAKES = 3;
	private readonly MAX_CONCURRENT_REQUESTS = 20;
	private readonly MAX_CONCURRENT_DELETE_REQUESTS = 3;
	private readonly MAX_SEARCH_RESULTS = 1000;

	private auth: Auth;

	constructor(private http: HttpClient) { }

	public setAuth(auth: Auth): void {
		this.auth = auth;
	}

	private getAuthHeaders(): HttpHeaders {
		return new HttpHeaders({
			Authorization: 'Basic ' + btoa(`${this.auth.username}:${this.auth.password}`)
		});
	}

	public createIssue(requestIssueBody: JiraIssueRequestBody): Observable<any> {

		const url = this.auth.host + this.ISSUES_ENDPOINT;

		return this.http
			.post(url, requestIssueBody, { headers: this.getAuthHeaders() })
			.pipe(
				retryWhen(errors => {
					return errors.pipe(
						delay(this.RETRY_DELAY),
						take(this.RETRY_TAKES)
					);
				})
			);
	}

	public updateIssueStatus(issueKey, statusTransitionId): Observable<any> {

		const url = this.auth.host + this.ISSUES_ENDPOINT + issueKey + this.TRANSITIONS_ENDPOINT;

		const requestBody = {
			transition: {
				id: statusTransitionId
			}
		}

		return this.http
			.post(url, requestBody, { headers: this.getAuthHeaders() })
			.pipe(
				retryWhen(errors => {
					return errors.pipe(
						delay(this.RETRY_DELAY),
						take(this.RETRY_TAKES)
					);
				})
			);;
	}

	public updateIssue(issueKey, requestIssueBody: JiraIssueRequestBody): Observable<any> {

		const url = this.auth.host + this.ISSUES_ENDPOINT + issueKey;

		return this.http.put(url, requestIssueBody, { headers: this.getAuthHeaders() });
	}

	public updateAdessoKey(issueKey, adessoKeyCustomFieldName, adessoKey): Observable<any> {

		const url = this.auth.host + this.ISSUES_ENDPOINT + issueKey;

		const requestBody = { fields: {} };
		requestBody.fields[adessoKeyCustomFieldName] = this.ADESSO_JIRA_BROWSE + adessoKey

		return this.http.put(url, requestBody, { headers: this.getAuthHeaders() }).pipe(
			retryWhen(errors => {
				return errors.pipe(
					delay(this.RETRY_DELAY),
					take(this.RETRY_TAKES)
				);
			})
		);
	}

	public deleteAllAttachments(attachmentIds): Observable<any> {

		const url = this.auth.host + this.ATTACHMENTS_ENDPOINT;

		return from(attachmentIds).pipe(
			mergeMap(id => {
				return this.http.delete(url + id, { headers: this.getAuthHeaders() });
			}, this.MAX_CONCURRENT_DELETE_REQUESTS)
		);
	}

	public getAllIssues(fields?: string[]): Observable<object[]> {

		const url = this.auth.host + this.SEARCH_ENDPOINT;

		let params = new HttpParams().append('maxResults', this.MAX_SEARCH_RESULTS.toString());

		if (fields && fields.length > 0) {
			params = params.append('fields', fields.toString());
		}

		const results = [];

		return new Observable(observer => {
			this.http.get(url, { headers: this.getAuthHeaders(), params })
				.subscribe((firstResponse: { total: number, maxResults: number, issues: object[] }) => {

					if (firstResponse.total < firstResponse.maxResults) {
						results.push(...firstResponse.issues);
						observer.next(results);
					} else {
						const numPages = [
							...Array(
								Math.ceil(firstResponse.total / firstResponse.maxResults)
							).keys()
						];
						from(numPages)
							.pipe(
								mergeMap<any, Observable<any>>(pageNum => {
									const startAt = pageNum * firstResponse.maxResults;
									params = params.set('startAt', startAt.toString());
									return this.http.get(url, { headers: this.getAuthHeaders(), params });
								}, this.MAX_CONCURRENT_REQUESTS)
							)
							.subscribe(
								result => {
									results.push(...result.issues);
								},
								err => { },
								() => {
									observer.next(results);
									observer.complete();
								}
							);
					}
				});
		});
	}
}

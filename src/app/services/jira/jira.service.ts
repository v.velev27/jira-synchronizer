import { Injectable } from '@angular/core';
import { delay, take, retryWhen } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { JiraClient } from './jira-client';
import { AdessoMapper } from '../mappers/adesso-mapper';
import { AbstractMapper } from '../mappers/mapper-abstract';
import { SecoMapper } from '../mappers/seco-mapper';
import { Auth } from 'app/domain/auth';
import { SyncModes } from 'app/domain/sync-modes.enum';

@Injectable({
	providedIn: 'root'
})
export class JiraService {

	private mode: SyncModes;
	private mapper: AbstractMapper;

	private readonly RETRY_DELAY = 500;
	private readonly RETRY_TAKES = 3;

	constructor(private jira: JiraClient) { }

	createIssue(inputIssue) {
		return new Observable(observer => {

			const requestIssueBody = this.mapper.mapToCreateIssueRequestBody(inputIssue);
			this.jira.createIssue(requestIssueBody)
				.subscribe(newIssue => {
					this.jira.updateIssueStatus(newIssue.key, this.mapper.mapStatus(inputIssue.Status))
						.subscribe(() => {
							observer.next({ newIssue, requestIssue: inputIssue });
							observer.complete();
						});
				});
		});
	}

	updateIssue(existingIssue, inputIssue) {
		return new Observable(observer => {
			const requests = [
				this.jira.updateIssue(existingIssue.key, this.mapper.mapToUpdateIssueRequestBody(existingIssue, inputIssue))
			];

			if (
				inputIssue.Status &&
				!this.mapper.isStatusMapped(existingIssue, inputIssue)
			) {
				requests.push(this.jira.updateIssueStatus(existingIssue.key, this.mapper.mapStatus(inputIssue.Status)))
			}

			forkJoin(requests)
				.pipe(
					retryWhen(errors => {
						console.log(errors);
						return errors.pipe(
							delay(this.RETRY_DELAY),
							take(this.RETRY_TAKES)
						);
					})
				)
				.subscribe(
					() => { },
					err => { },
					() => {
						observer.next({ key: existingIssue.key, requestIssue: inputIssue });
						observer.complete();
					}
				);
		});
	}

	updateAdessoKey(existingIssue, inputIssue) {

		const adessoCustomKey = 'customfield_10028';

		return this.jira.updateAdessoKey(existingIssue.key, adessoCustomKey, inputIssue['Issue key'])
	}

	setMode(mode: SyncModes) {
		this.mode = mode;
		this.setMapper();
	}

	getMode(): SyncModes {
		return this.mode;
	}

	setMapper() {
		switch (this.mode) {
			case SyncModes.ADESSO_SECO:
				this.mapper = new SecoMapper();
				break;
			case SyncModes.SECO_ADESSO:
				this.mapper = new AdessoMapper();
				break;
		}
	}

	deleteAllAttachments(attachmentIds: Array<any>) {
		return this.jira.deleteAllAttachments(attachmentIds);
	}

	getAllIssues(fields?): Observable<any[]> {
		return this.jira.getAllIssues(fields);
	}

	setAuth(auth: Auth) {
		this.jira.setAuth(auth);
	}
}

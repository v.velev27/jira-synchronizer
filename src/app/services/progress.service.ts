import { Injectable } from '@angular/core';

export enum ProgressStatus {
	LOADING = 'loading',
	DONE = 'done',
	ERROR = 'error'
}

@Injectable({
	providedIn: 'root'
})
export class ProgressService {

	private progressPoints = [
		{
			key: 'loadingExisting',
			status: 'done',
			string: 'Loading existing issues',
			counter: null,
			maxCounter: null,
			enabled: true
		},
		{
			key: 'mapping',
			status: 'done',
			string: 'Mapping issues',
			counter: null,
			maxCounter: null,
			enabled: true
		},
		{
			key: 'createIssues',
			status: 'done',
			string: 'Creating new issues',
			counter: 100,
			maxCounter: 100,
			enabled: true
		},
		{
			key: 'updateIssues',
			status: 'done',
			string: 'Updating existing issues',
			counter: 200,
			maxCounter: 200,
			enabled: true
		},
		{
			key: 'downloadAttachments',
			status: 'done',
			string: 'Downloading attachments',
			counter: 20,
			maxCounter: 20,
			enabled: true
		},
		{
			key: 'uploadAttachments',
			status: 'loading',
			string: 'Uploading attachments',
			counter: 21,
			maxCounter: 30,
			enabled: true
		},
		{
			key: 'updateSecoIssues',
			status: 'done',
			string: 'Updating Jira Adesso links in Jira SECO',
			counter: null,
			maxCounter: null,
			enabled: false
		}
	];

	constructor() { }

	get() {
		return this.progressPoints;
	}

	set(key: string, status: ProgressStatus, counter = null, maxCounter = null, enabled = true) {
		this.getProgressPoint(key).status = status;
		this.getProgressPoint(key).counter = counter;
		this.getProgressPoint(key).maxCounter = maxCounter;
		this.getProgressPoint(key).enabled = enabled;
	}

	increment(key) {
		this.getProgressPoint(key).counter++;
	}

	reset() {
		this.progressPoints = this.progressPoints.map(point => {
			point.status = '';
			point.counter = null;
			point.maxCounter = null;
			return point;
		});
	}

	private getProgressPoint(key) {
		return this.progressPoints.find(point => point.key === key);
	}
}

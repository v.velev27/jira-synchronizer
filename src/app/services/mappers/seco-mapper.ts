import { AbstractMapper } from './mapper-abstract';

export class SecoMapper implements AbstractMapper {

    private readonly mappingReporter = {
        kirilova: 'TYFWC',
        dicheva: 'TYFWB',
        'paul.bloch': 'TYFTT'
    };

    private readonly mappingPriority = {
        Blocker: 'Hoch',
        Critical: 'Mittel',
        Major: 'Niedrig',
        Minor: 'Niedrig',
        Trivial: 'Niedrig'
    };

    private readonly mappingStatus = {
        'Userstory Ready for Development': 11, // 'Backlog',
        'Ready for Development': 11, // 'Backlog',
        'To Do': 11, // 'Backlog',
        'To Correct': 11, // 'Backlog',
        'Not Ready': 11, // 'Backlog',
        'In Progress': 31, // 'In Progress',
        'To Verify': 31, // 'In Progress',
        'In Review': 31, // 'In Progress',
        'Done': 41, // 'Done'
    };

    mapToCreateIssueRequestBody(inputIssue: any) {
        const requestIssue = {
            fields: {
                project: {
                    key: 'CRAMRE3'
                },
                summary: inputIssue.Summary,
                issuetype: {
                    name: 'Bug'
                },
                description: inputIssue.Description,
                priority: {
                    name: this.mappingPriority[inputIssue.Priority]
                },
                customfield_10028: 'https://ticketing.adesso.ch/browse/' + inputIssue['Issue key']
            }
        };

        if (this.mappingReporter[inputIssue.Reporter]) {
            requestIssue.fields['reporter'] = {
                name: this.mappingReporter[inputIssue.Reporter].toLowerCase()
            };
        }

        return requestIssue;
    }

    mapToUpdateIssueRequestBody(existingIssue: any, inputIssue: any) {
        const requestIssue = {
            fields: {
                summary: inputIssue.Summary,
                description: inputIssue.Description,
                priority: {
                    name: this.mappingPriority[inputIssue.Priority]
                },
            }
        };

        return requestIssue;
    }

    mapStatus(inputStatus) {
        return this.mappingStatus[inputStatus]
    }

    isStatusMapped(existingIssue, inputIssue) {
        return this.mappingStatus[inputIssue.Status];
    }
}
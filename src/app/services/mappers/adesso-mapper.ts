import { AbstractMapper } from "./mapper-abstract";

export class AdessoMapper implements AbstractMapper {
    private readonly mappingReporter = {
        TYFWC: "kirilova",
        TYFWB: "dicheva",
        TYFTT: "paul.bloch"
    };

    private readonly mappingPriority = {
        Hoch: ["Blocker"],
        Mittel: ["Critical"],
        Niedrig: ["Major", "Minor", "Trivial"]
    };

    private readonly statusIds = {
        "Userstory Ready for Development": 11,
        "In Progress": 21,
        Done: 31
    };

    private readonly mappingStatus = {
        Open: [
            "Userstory Ready for Development",
            "Ready for Development",
            "To Do",
            "To Correct",
            "Not Ready"
        ],
        Bearbeitung: ["In Progress", "To Verify", "In Review"],
        Abgeschlossen: ["Done"]
    };

    mapToCreateIssueRequestBody(inputIssue) {
        const requestIssue = {
            fields: {
                project: {
                    key: "AVB"
                },
                summary: inputIssue.Summary,
                issuetype: {
                    name: "Bug"
                },
                description: inputIssue.Description,
                priority: {
                    name: this.mappingPriority[inputIssue.Priority][0]
                },
                components: [
                    {
                        name: "02 - Entwicklung"
                    }
                ],
                customfield_10020: 3,
                customfield_10029: {
                    value: "(4) Business requirements"
                },
                customfield_10026: 0.5,
                customfield_10030: {
                    value: "Intern & Payroller"
                }
            }
        };

        if (this.mappingReporter[inputIssue.Reporter.toUpperCase()]) {
            requestIssue.fields["reporter"] = {
                name: this.mappingReporter[inputIssue.Reporter.toUpperCase()]
            };
        }

        return requestIssue;
    }

    mapToUpdateIssueRequestBody(existingIssue, inputIssue) {
        const requestIssueBody = {
            fields: {
                summary: inputIssue.Summary,
                description: inputIssue.Description
            }
        };

        if (
            inputIssue.Priority &&
            !this.mappingPriority[inputIssue.Priority].includes(
                existingIssue["fields"]["priority"]["name"]
            )
        ) {
            requestIssueBody["fields"]["priority"] = {
                name: this.mappingPriority[inputIssue.Priority][0]
            };
        }

        return requestIssueBody;
    }

    mapStatus(inputStatus) {
        return this.statusIds[this.mappingStatus[inputStatus][0]];
    }

    isStatusMapped(existingIssue, requestIssue) {
        this.mappingStatus[requestIssue.Status].includes(
            existingIssue["fields"]["status"]["name"]
        );
    }
}

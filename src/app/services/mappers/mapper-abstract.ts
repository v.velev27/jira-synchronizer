export abstract class AbstractMapper {
    mapToCreateIssueRequestBody(inputIssue): any { }
    mapToUpdateIssueRequestBody(existingIssue, inputIssue): any { }
    mapStatus(inputStatus): any { }
    isStatusMapped(existingIssue, inputIssue): any { }
}

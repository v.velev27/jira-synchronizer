import { Injectable } from '@angular/core';
import FileDataLoader from '../utils/file-data-loader';
import { from, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { AttachmentsTransferService } from './attachments/attachments-transfer.service';
import { AttachmentsDBService } from './attachments/attachments-db.service';
import { JiraService } from './jira/jira.service';
import { Attachment } from '../domain/attachment';
import { ProgressService, ProgressStatus } from './progress.service';
import { Auth } from 'app/domain/auth';

@Injectable({
    providedIn: 'root'
})
export class AdessoSecoImportService {
    private readonly MAX_CONCURRENT_REQUESTS = 20;

    private sourceAuth: Auth;
    private targetAuth: Auth;

    private exportFile: File;

    constructor(
        private attachmentsSvc: AttachmentsTransferService,
        private attachmentsDbSvc: AttachmentsDBService,
        private jiraSvc: JiraService,
        private progress: ProgressService
    ) { }

    setExportAuth(exportAuth: Auth) {
        this.sourceAuth = exportAuth;
    }

    setImportAuth(importAuth: Auth) {
        this.targetAuth = importAuth;
    }

    setExportfile(file: File) {
        this.exportFile = file;
    }

    getExportFile() {
        return this.exportFile;
    }

    async import() {
        let existingIssues: object[] = [];
        let uploadAttachmentsMeta: Attachment[] = [];
        let existingKeys: string[] = [];
        let exportIssues: object[] = [];
        let attachmentIdsForDelete: string[] = [];

        // setting auth (username, password, host)
        this.jiraSvc.setAuth(this.targetAuth);

        // loading the existing issues from the target Jira
        this.progress.set('loadingExisting', ProgressStatus.LOADING);
        const issues = await this.loadExistingIssues();
        existingIssues = issues;
        // extracting the keys of the existing issues in jira adesso
        existingKeys = existingIssues.map(issue => this.extractAdessoKey(issue));
        this.progress.set('loadingExisting', ProgressStatus.DONE);

        // lading objects from file
        this.progress.set('mapping', ProgressStatus.LOADING);
        const fileIssues = await FileDataLoader.load(this.exportFile);
        exportIssues = fileIssues;
        // filtering the issues for create and update
        const issuesForCreate = exportIssues.filter(issue => !existingKeys.includes(issue['Issue key']));
        const issuesForUpdate = exportIssues.filter(issue => existingKeys.includes(issue['Issue key']));
        // getting the attachment ids that need to be deleted
        attachmentIdsForDelete = this.extractAttachmentIdsForDelete(existingIssues, issuesForUpdate);
        this.progress.set('mapping', ProgressStatus.DONE);


        // creating issues
        this.progress.set('createIssues', ProgressStatus.LOADING, 0, issuesForCreate.length);
        const createAttachmentsMeta = await this.createIssues(issuesForCreate);
        // saving the attachments meta
        uploadAttachmentsMeta = uploadAttachmentsMeta.concat(createAttachmentsMeta);
        this.progress.set('createIssues', ProgressStatus.DONE);


        // updating issues
        this.progress.set('updateIssues', ProgressStatus.LOADING, 0, issuesForUpdate.length);
        const attachmentsMetaUpdate = await this.updateIssues(issuesForUpdate, existingIssues);
        // filtering only non-existing attachments
        const updateAttachmentsMeta: Attachment[] = attachmentsMetaUpdate.filter((attachment: Attachment) =>
            !existingIssues
                .find(existingIssue => existingIssue['key'] === attachment.issueKey)['fields']['attachment']
                .find(issueAttachment => issueAttachment['filename'] === attachment.fileName));
        // saving those attachment metas
        uploadAttachmentsMeta = uploadAttachmentsMeta.concat(updateAttachmentsMeta);
        this.progress.set('updateIssues', ProgressStatus.DONE)


        // downloading attachments
        this.progress.set('downloadAttachments', ProgressStatus.LOADING, 0, uploadAttachmentsMeta.length)
        await Promise.all([this.downloadAttachments(uploadAttachmentsMeta), this.deleteUpdateAttachments(attachmentIdsForDelete)]);
        // getting attachment references from DB
        const attachments = await this.attachmentsDbSvc.getAll();
        this.progress.set('downloadAttachments', ProgressStatus.DONE);


        // uploading attachments
        this.progress.set('uploadAttachments', ProgressStatus.LOADING, 0, attachments.length);
        await this.uploadAttachments(attachments);
        this.progress.set('uploadAttachments', ProgressStatus.DONE);

        console.log('All done!');
    }

    loadExistingIssues(): Promise<object[]> {
        return this.jiraSvc.getAllIssues(['customfield_10028', 'attachment', 'priority', 'status']).toPromise();
    }

    createIssues(exportIssues: object[]): Promise<Attachment[]> {
        return new Promise((resolve) => {
            let issuesAttachmentsMeta = [];
            // convert issue objects to Observable values
            from(exportIssues)
                // create create requests and merge them into a single observable
                .pipe(
                    mergeMap<any, Observable<any>>((issue) => {
                        return this.jiraSvc.createIssue(issue);
                    }, this.MAX_CONCURRENT_REQUESTS)
                ).subscribe(response => {
                    this.progress.increment('createIssues');

                    // extract attachments metadata for each issue
                    let issueAttachmentsMeta = this.extractIssueAttachmentsMeta(response.requestIssue);

                    if (issueAttachmentsMeta) {
                        issueAttachmentsMeta.forEach(attachment => attachment['issue'] = response.newIssue['key']);
                        issuesAttachmentsMeta = issuesAttachmentsMeta.concat(issueAttachmentsMeta);
                    }

                }, err => { },
                    () => {
                        resolve(issuesAttachmentsMeta);
                    });
        });
    }

    updateIssues(exportIssues: object[], existingIssues: object[]): Promise<Attachment[]> {
        return new Promise((resolve) => {
            let issuesAttachmentsMeta = [];

            // convert issue objects to Observable values
            from(exportIssues)
                // create update requests and merge them into a single observable
                .pipe(
                    mergeMap<any, Observable<any>>((issue) => {
                        // tslint:disable-next-line: max-line-length
                        const existingIssue = existingIssues.find(existingIssue => this.extractAdessoKey(existingIssue) === issue['Issue key']);
                        return this.jiraSvc.updateIssue(existingIssue, issue);
                    }, this.MAX_CONCURRENT_REQUESTS / 2)
                ).subscribe(response => {
                    this.progress.increment('updateIssues');
                    const issueAttachmentsMeta = this.extractIssueAttachmentsMeta(response.requestIssue);

                    if (issueAttachmentsMeta) {
                        issueAttachmentsMeta.forEach((attachment: Attachment) => attachment.issueKey = response['key']);
                        issuesAttachmentsMeta = issuesAttachmentsMeta.concat(issueAttachmentsMeta);
                    }

                }, err => { },
                    () => {
                        resolve(issuesAttachmentsMeta);
                    });
        });
    }

    deleteUpdateAttachments(ids: string[] | number[]): Promise<any> {

        return this.jiraSvc.deleteAllAttachments(ids).toPromise();
    }

    downloadAttachments(issuesAttachmentsMeta: Attachment[]) {

        return new Promise((resolve) => {
            this.attachmentsSvc.downloadAttachments(issuesAttachmentsMeta, this.sourceAuth)
                .subscribe(result => {
                    this.progress.increment('downloadAttachments');
                }, err => { }, resolve);
        });
    }

    uploadAttachments(attachments: Attachment[]) {

        return new Promise((resolve) => {
            this.attachmentsSvc.uploadAttachments(attachments, this.targetAuth)
                .subscribe(result => {
                    this.progress.increment('uploadAttachments');
                }, err => { }, resolve);
        });
    }

    extractIssueAttachmentsMeta(issue: any): Attachment[] {
        const attachmentsMeta: Attachment[] = [];
        if (issue.Attachment) {
            let attachmentString = issue.Attachment.split(';');
            attachmentsMeta.push({
                fileName: attachmentString[2],
                fileLink: attachmentString[3]
            });

            for (let i = 1; ; i++) {
                if (!issue['Attachment__' + i]) {
                    return attachmentsMeta;
                }

                attachmentString = issue['Attachment__' + i].split(';');
                attachmentsMeta.push({
                    fileName: attachmentString[2],
                    fileLink: attachmentString[3]
                });
            }
        } else {
            return attachmentsMeta;
        }
    }

    canImport() {
        return (this.sourceAuth && this.targetAuth && this.exportFile);
    }

    getProgress() {
        this.progress.get();
    }

    private extractAttachmentIdsForDelete(existingIssues: object[], issuesForUpdate: object[]) {
        return existingIssues
            .filter(issue => issuesForUpdate.find(
                issueForUpdate => issueForUpdate['Issue key'] === this.extractAdessoKey(issue)
                    && issue['fields']['attachment'].length > 0))
            .map(issue => issue['fields']['attachment'])
            .flat()
            .map(attachment => attachment['id']);
    }

    private extractAdessoKey(issue: object) {
        const adessoLink = issue['fields']['customfield_10028'];
        return adessoLink ? adessoLink.split('/').pop() : null;
    }
}

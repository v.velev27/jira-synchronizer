import { Component, OnInit } from '@angular/core';
import { AttachmentsDBService } from './services/attachments/attachments-db.service';
import { SecoAdessoImportService } from './services/seco-adesso-import.service';
import { AdessoSecoImportService } from './services/adesso-seco-import.service';
import { ProgressService } from './services/progress.service';
import { JiraService } from './services/jira/jira.service';
import { SyncModes } from './domain/sync-modes.enum';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'Jira Synchronizer';
	spinner = false;
	showProgress = false;
	importer = undefined;

	constructor(
		private attachmentsDB: AttachmentsDBService,
		private secoAdessoImporter: SecoAdessoImportService,
		private adessoSecoImporter: AdessoSecoImportService,
		private progress: ProgressService,
		private jira: JiraService
	) { }

	ngOnInit() {
		this.setImporter(SyncModes.ADESSO_SECO);
		this.attachmentsDB.deleteDb();
	}

	import() {
		this.attachmentsDB.clearDb().then(() => {
			this.progress.reset();
			this.spinner = true;
			this.showProgress = true;
			this.importer.import().then(() => {
				this.spinner = false;
			});
		});
	}

	setImporter(importerString) {
		this.showProgress = false;
		if (importerString === SyncModes.SECO_ADESSO) {
			this.importer = this.secoAdessoImporter;
			this.jira.setMode(SyncModes.SECO_ADESSO);
		} else if (importerString === SyncModes.ADESSO_SECO) {
			this.importer = this.adessoSecoImporter;
			this.jira.setMode(SyncModes.ADESSO_SECO);
		}
	}
}

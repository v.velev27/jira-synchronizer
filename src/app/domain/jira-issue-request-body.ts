export interface JiraIssueRequestBody {
    fields?: {
        project?: {
            key?: string
        },
        summary?: string,
        issuetype?: {
            name?: string
        },
        description?: string,
        priority?: {
            name?: string
        },
        reporter?: string
    }
}
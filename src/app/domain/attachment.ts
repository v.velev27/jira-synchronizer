export interface Attachment {
    fileName: string;
    fileLink?: string;
    issueKey?: string;
    data?: Blob;
}

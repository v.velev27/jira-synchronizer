export interface Auth {
    username: string;
    password: string;
    host?: string;
}

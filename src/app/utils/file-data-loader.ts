import * as XLSX from 'xlsx';
import CSVJSON from 'csvjson-csv2json';

export default class FileDataLoader {
    static fileReader: FileReader = new FileReader();

    private static loadFromXls(file: File): Promise<any> {

        return new Promise((resolve, reject) => {
            FileDataLoader.fileReader.onload = () => {

                const workbook: XLSX.WorkBook = XLSX.read(FileDataLoader.fileReader.result as Uint8Array, { type: 'array' });
                const firstSheet = workbook.Sheets[workbook.SheetNames[0]];

                resolve(XLSX.utils.sheet_to_json(firstSheet));
            };

            FileDataLoader.fileReader.readAsArrayBuffer(file);
        });
    }

    private static loadFromCsv(file: File): Promise<any> {

        return new Promise((resolve, reject) => {

            FileDataLoader.fileReader.onload = () => {
                resolve(CSVJSON.csv2json(FileDataLoader.fileReader.result.toString()));
            };
            FileDataLoader.fileReader.readAsText(file);
        });
    }

    private static loadFromJson(file: File): Promise<any> {
        return new Promise((resolve, reject) => {

            FileDataLoader.fileReader.onload = () => {
                resolve(JSON.parse(FileDataLoader.fileReader.result.toString()));
            };
            FileDataLoader.fileReader.readAsText(file);
        });
    }

    public static load(file: File): Promise<any> {
        const fileExtension = file.name.split('.').pop();
        let loader = null;

        switch (fileExtension) {
            case 'xls':
            case 'xlsx':
                loader = FileDataLoader.loadFromXls;
                break;
            case 'csv':
                loader = FileDataLoader.loadFromCsv;
                break;
            case 'json':
                loader = FileDataLoader.loadFromJson;
                break;
        }

        return loader(file);
    }
}
